const env = {
    active: 'dev',//preview,dev
    baseUrl: '/design',
    fileUrl: '/fileUrl',
    version: '1.8.5',
}
export const baseUrl = env.baseUrl
export const fileUrl = env.fileUrl
export default env
